#!/usr/bin/env python

import os
import glob

from gi.repository import GExiv2

MOIS = ('janvier','fevrier','mars','avril','mai','juin','juillet','aout','septembre','octobre','novembre','decembre')

for f in glob.glob('*'):
    print f, ':',
    try:
        date = GExiv2.Metadata(f).get_date_time()
    except:
        print 'no date'
        continue
    dir = '%d/%s' % (date.year, MOIS[date.month-1])
    try:
        os.makedirs(dir)
    except:
        pass
    os.rename(f, os.path.join(dir, f))
    print 'moved to', dir
